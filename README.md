# Pressure-Temperature Sensor Layer

The tiny little board with the pressure/temperature sensor.

The board holds our gel sensor, the MS5803-14BA. This sensor was chosen because of its low power requirement and precision readings. Not located as part of the stack, the board will be at the edge of the epoxy with the gel sensor exposed. We only need 4 headers to retrieve data from the sensor: serial clock (SCL), serial data (SDA), ground, and power.